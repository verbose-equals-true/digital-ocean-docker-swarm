version: "3.7"

services:
  postgres:
    container_name: postgres
    image: postgres:11.5
    networks:
      - main
    ports:
      - "5434:5432"
    volumes:
      - pg-data:/var/lib/postgresql/data
    environment:
      - POSTGRES_PASSWORD=postgres

  pgadmin:
    container_name: pgadmin
    image: dpage/pgadmin4:4.25
    environment:
      PGADMIN_DEFAULT_EMAIL: pgadmin4@pgadmin.org
      PGADMIN_DEFAULT_PASSWORD: password
    volumes:
      - pgadmin:/root/.pgadmin
    ports:
      - "8432:80"
    depends_on:
      - "postgres"
    networks:
      - main
    restart: unless-stopped

  redis:
    image: redis:alpine
    volumes:
      - redis-data:/data
    container_name: redis
    networks:
      - main

  rediscommander:
    container_name: rediscommander
    image: rediscommander/redis-commander:latest
    restart: always
    environment:
      - REDIS_HOST=redis
    ports:
      - "8085:8081"
    networks:
      - main

  nginx:
    container_name: nginx
    build:
      context: ./nginx
      dockerfile: dev/Dockerfile
    ports:
      - "80:80"
    depends_on:
      - frontend
      - backend
    volumes:
      - ./nginx/dev/dev.conf:/etc/nginx/nginx.conf:ro
    networks:
      - main
    logging:
      driver: none

  frontend:
    container_name: frontend
    build:
      context: ./quasar
    networks:
      - main
    ports:
      - "8080:8080"
    volumes:
      - ./quasar:/app/:rw

  backend: &backend
    container_name: backend
    build:
      context: ./backend
      dockerfile: docker/Dockerfile.dev
    command:
      - "python3"
      - "manage.py"
      - "runserver_plus"
      - "0.0.0.0:8000"
    volumes:
      - ./backend:/code
    networks:
      - main
    ports:
      - "8000:8000"
      - "8888:8888"
    environment:
      - DJANGO_SETTINGS_MODULE=backend.settings.development
      - SECRET_KEY=secret
      - DEBUG=1
    depends_on:
      - postgres
      - redis

  celery:
    <<: *backend
    container_name: celery
    command:
      - "watchmedo"
      - "auto-restart"
      - "--directory=./"
      - "--pattern=*.py"
      - "--recursive"
      - "--"
      - "celery"
      - "worker"
      - "--app=backend.celery_app:app"
      - "-Q"
      - "default"
      - "-n"
      - "other@%h"
      - "--concurrency=1"
      - "--loglevel=INFO"
    ports: []

  celery_other:
    <<: *backend
    container_name: celery_other
    command:
      - "watchmedo"
      - "auto-restart"
      - "--directory=./"
      - "--pattern=*.py"
      - "--recursive"
      - "--"
      - "celery"
      - "worker"
      - "--app=backend.celery_app:app"
      - "-Q"
      - "other"
      - "-n"
      - "other@%h"
      - "--concurrency=1"
      - "--loglevel=INFO"
    ports: []

  beat:
    <<: *backend
    container_name: beat
    command:
      - "sh"
      - "-c"
      - |
        # remove celerybeat.pid and celerybeat-schedule if they exist
        rm -f /code/celerybeat*;
        watchmedo \
          auto-restart \
          --directory=./ \
          --pattern=*.py \
          --recursive \
          -- \
          celery \
          beat \
          --app=backend.celery_app:app \
          --loglevel=INFO \
          --pidfile=/code/celerybeat.pid
    volumes:
      - ./backend:/code
    ports: []

  flower:
    image: mher/flower:latest
    container_name: flower
    command: --url_prefix=flower
    environment:
      - CELERY_BROKER_URL=redis://redis:6379/1
      - FLOWER_PORT=5555
    ports:
      - 5555:5555
    networks:
      - main
    depends_on:
      - redis
    logging:
      driver: none

  mailhog:
    container_name: mailhog
    image: mailhog/mailhog
    ports:
      - 1025:1025
      - 8025:8025
    networks:
      - main

volumes:
  pg-data:
  pgadmin:
  django-static:
  redis-data:

networks:
  main:
    driver: bridge
