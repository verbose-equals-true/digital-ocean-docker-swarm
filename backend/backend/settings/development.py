from .base import *  # noqa

DEBUG_APPS = ["debug_toolbar", "django_extensions"]

INSTALLED_APPS = DEBUG_APPS + INSTALLED_APPS

MIDDLEWARE = ["debug_toolbar.middleware.DebugToolbarMiddleware",] + MIDDLEWARE


def show_toolbar(request):
    return True


DEBUG_TOOLBAR_CONFIG = {
    "SHOW_TOOLBAR_CALLBACK": show_toolbar,
}

# for jupyter notebooks
NOTEBOOK_ARGUMENTS = [
    '--ip',
    '0.0.0.0',
    '--allow-root',
    '--no-browser',
]
