"""
This file demonstrates different ways to declare tasks

Task routing is controlled through Django settings, see CELERY_TASK_ROUTES

https://docs.celeryproject.org/en/stable/userguide/tasks.html

https://docs.celeryproject.org/en/stable/userguide/routing.html

"""

import time

import celery

from backend.celery_app import app
from celery import shared_task
from celery.task import task
from django.conf import settings


@app.task
def debug_task():
    """
    This task is processed by the default queue
    """
    time.sleep(3)
    return "Task is done."


@app.task
def debug_task_other():
    """
    This task is processed by the other queue
    """
    time.sleep(3)
    return "Task is done for other queue."


@app.task
def debug_task_default_queue():
    """
    This task is not defined in CELERY_TASK_ROUTES,
    so it is processed by the default queue
    """
    return "Task should be processed by default queue"


@app.task(name="named_task")
def my_named_task():
    """
    This task is named, and referenced by ``named_task`` in
    CELERY_TASK_ROUTES
    """
    time.sleep(3)
    return "Should be processed by the other queue"


@task
def my_task():
    """
    This task uses another decorator: ``from celery.task import task``
    """
    time.sleep(3)
    return "New task done!"


@shared_task
def my_shared_task():
    """
    This task uses the ``shared_task`` decorator
    """
    time.sleep(3)
    return "Shared task has completed."


# TODO: add BaseTask examples
# also show delay and apply_async in views.py
